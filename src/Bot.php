<?php

namespace Egweb\Telegram;

use Exception;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\InvalidArgumentException;

class Bot
{
    private BotApi $bot;

    /**
     * @throws Exception
     */
    public function __construct(
        private readonly string $token,
        private readonly array $allowedUsers
    ) {
        $this->bot = new BotApi($this->token);
    }

    public function notifyError(string $message): void
    {
        $type = '⚠️ERROR⚠️';
        foreach ($this->allowedUsers as $allowedUser) {
            try {
                $this->bot->sendMessage(
                    $allowedUser,
                    $type.'\n'.$message
                );
            } catch (\TelegramBot\Api\Exception|InvalidArgumentException $e) {
                file_put_contents(__DIR__ . '/telegram.log', $e);
            }
        }
    }
}